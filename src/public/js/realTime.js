$(document).ready(()=>{
    const socket = io();
    
    socket.on('nodeMCUMQ5', function(dataReceiver){
 
        
        let fecha = new Date(dataReceiver.hora);

        let hora = fecha.getHours();
        let minutos = fecha.getMinutes();
        let segundos = fecha.getSeconds();

        let fechaActual = `${hora}:${minutos}:${segundos}`;
        console.log(fechaActual);

        myChart.data.labels.push(fechaActual);
        
        if(myChart.data.labels.length > 10){
            myChart.data.labels.shift();
        }
        myChart.data.datasets.forEach((element, index) => {
            element.data.push(dataReceiver.valor);
            if(element.data.length > 10){
                element.data.shift();       
            }       
        });
        
        
        
        myChart.update();
    });

    var ctx = document.getElementById('myCanvas').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            
            labels:[], //Pasa el tiempo en un array
            
            datasets: [{
                label: 'Gas level',
                data: [], //para el nivel de gas en un array
                backgroundColor: 'rgba(0, 0, 0, 0.1)',
                borderColor: 'rgba(240, 15, 39)',
                pointRadius: 5,
                pointBackgroundColor: 'rgba(14, 1, 10)',
                pointHoverRadius: 8,
                pointHoverBackgroundColor: 'rgba(200, 9, 35)',
            }]
        },
        
        options: {
            title: {
            display: true,
            text: 'Grafica Nivel de Gas',
            fontSize: 25,
            padding: 10
        },
        layout: {
        padding: {
            left: 130,
            right: 130,
            top: 50,
            bottom: 0
        }
        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0
                    }
                }]
            }
        }
    });

            
    socket.on('Dradoric', function(dataReceiver){
    
            
        let fecha = new Date(dataReceiver.hora);

        let hora = fecha.getHours();
        let minutos = fecha.getMinutes();
        let segundos = fecha.getSeconds();

        let fechaActual = `${hora}:${minutos}:${segundos}`;
        console.log(fechaActual);

        myChartVT.data.labels.push(fechaActual);
        
        if(myChartVT.data.labels.length > 10){
            myChartVT.data.labels.shift();
        }
        myChartVT.data.datasets.forEach((element, index) => {
            element.data.push(dataReceiver.valor);
            if(element.data.length > 10){
                element.data.shift();       
            }       
        });
        
        
        
        myChartVT.update();
    });

    var ctx = document.getElementById('myCanvasVT').getContext('2d');
    var myChartVT = new Chart(ctx, {
        type: 'line',
        data: {
            
            labels:[], //Pasa el tiempo en un array
            
            datasets: [{
                label: 'Gas level',
                data: [], //para el nivel de gas en un array
                backgroundColor: 'rgba(0, 0, 0, 0.1)',
                borderColor: 'rgba(240, 15, 39)',
                pointRadius: 5,
                pointBackgroundColor: 'rgba(14, 1, 10)',
                pointHoverRadius: 8,
                pointHoverBackgroundColor: 'rgba(200, 9, 35)',
            }]
        },
        
        options: {
            title: {
            display: true,
            text: 'Grafica Temperatura',
            fontSize: 25,
            padding: 10
        },
        layout: {
        padding: {
            left: 130,
            right: 130,
            top: 50,
            bottom: 0
        }
        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0
                    }
                }]
            }
        }
});

    
});