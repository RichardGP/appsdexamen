const mqtt = require('mqtt');
const express = require('express');
const socket = require('socket.io');
const http = require('http');

const client = mqtt.connect('mqtt://localhost');

const app = express();
const server = http.createServer(app);
const io = socket.listen(server);
const connection = require('./connection/getConnection.js');

client.on('connect', ()=>{
    client.subscribe('#',(err)=>{
        if(!err){
            client.on('message', (topic, message)=>{
                if(topic == 'nodeMCUMQ5'){
                    console.log('Mensaje recibido: ' + message.toString());
                    
                    let msg = message.toString().split("$");

                    //'2030-12-2 23:59:59'
                    let fecha = msg[0];
                    let valor = msg[1];
                    let ubicacion= msg[2];              
            

                    connection((err, conn)=>{
                        if(!err){
                            let sql = `INSERT INTO BDsensor.GENERAL(id_grupo, valor, ubicacion, hora) VALUES('${topic}', ${valor},'${ubicacion}','${fecha}' );`;
                            console.log(topic);
                            conn.query(sql, ((err, res)=>{
                                if(!err){
                                    consultar(topic);
                                }else{
                                    throw err;
                                }
                            }));
                
                            console.log('registro guardado');
                        }
                    });
                }
                if(topic == 'sensorozono'){
                    console.log('Mensaje recibido: ' + message.toString());
                    
                    let msg = message.toString().split("$");
    
                    //'2030-12-2 23:59:59'
                    let fecha = msg[0];
                    let valor = msg[1];
                    let ubicacion= msg[2];              
              
    
                    connection((err, conn)=>{
                        if(!err){
                            let sql = `INSERT INTO BDsensor.GENERAL(id_grupo, valor, ubicacion, hora) VALUES('${topic}', ${valor},'${ubicacion}','${fecha}' );`;
                            console.log(topic);
                            conn.query(sql, ((err, res)=>{
                                if(!err){
                                    console.log('Insertado');
                                    consultar(topic);
                                }else{
                                    throw err;
                                }
                            }));
                
                        
                        }
                    });     

                }     
                if(topic == 'Dradoric'){
                    console.log('Mensaje recibido: ' + message.toString());
                    console.log(topic);
                    let msg = message.toString().split("$");
    
                    //'2030-12-2 23:59:59'
                    let fecha = msg[0];
                    let valor = msg[1];
                    let ubicacion= msg[2];              
              
    
                    connection((err, conn)=>{
                        if(!err){
                            let sql = `INSERT INTO BDsensor.GENERAL(id_grupo, valor, ubicacion, hora) VALUES('${topic}', ${valor},'${ubicacion}','${fecha}' );`;
                            conn.query(sql, ((err, res)=>{
                                if(!err){
                                    consultar(topic);
                                }else{
                                    throw err;
                                }
                            }));
                
                        
                        }
                    });     

                }     
            });


        }
    });

   
});

app.use(express.static(__dirname+'/public'));

app.get('/', (req, res)=>{
    res.sendFile(__dirname+'/public/index.html');

});

function consultar(parametro){
    connection((err, conn)=>{
        if(!err){
            conn.query(`SELECT * FROM bdsensor.general where id_grupo = '${parametro}' order by hora desc limit 1`, (err,gas)=>{
                if(err){
                    throw err;
                }else{
                    let valores = JSON.parse(JSON.stringify(gas));
                    for(let valor in valores){
                        io.emit(parametro,valores[valor]);
                        console.log(valores[valor]);
                    }
                }
            });
        }
    });
}


server.listen(3000,()=>{
    console.log('Server on port ',3000);
});